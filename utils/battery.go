package utils

import (
	"github.com/distatus/battery"
)

func GetBatPercentage() int {
	batteries, err := battery.GetAll()
	if err != nil {
		return -1
	}
	current := batteries[0].Current
	full := batteries[0].Full

	return int((current / full) * 100)
}
