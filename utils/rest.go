package utils

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type locationData struct {
	IDT      string `'json:"idt"`
	Provider string `'json:"provider"`
	Date     uint64 `'json:"date"`
	Bat      string `'json:"bat"`
	Lon      string `json:"lon"`
	Lat      string `json:"lat"`
}

type registrationData struct {
	HashedPassword string `'json:"hashedPassword"`
	PubKey         string `'json:"pubKey"`
	PrivKey        string `'json:"privKey"`
}

// universal package for string transfer
// IDT = DeviceID or AccessToken
// If both will be send. ID is always IDT
type DataPackage struct {
	IDT  string `'json:"Identifier"`
	Data string `'json:"Data"`
}

type AccessToken struct {
	DeviceId    string
	AccessToken string
	Time        int64
}

func RegisterDevice(url, publicKey, encryptedPrivateKey, hashedPassword string) string {
	client := &http.Client{}
	regData := registrationData{HashedPassword: hashedPassword, PubKey: publicKey, PrivKey: encryptedPrivateKey}
	dataAsString, _ := json.Marshal(regData)
	request, _ := http.NewRequest("PUT", url+"/device", bytes.NewBuffer(dataAsString))

	r, _ := client.Do(request)
	defer r.Body.Close()
	var data AccessToken
	json.NewDecoder(r.Body).Decode(&data)
	return string(data.DeviceId)
}

func PostLocation(url, at, provider, bat, lon, lat string, date int64) {
	client := &http.Client{}
	locData := locationData{IDT: at, Provider: provider, Date: uint64(date), Bat: bat, Lon: lon, Lat: lat}
	dataAsString, _ := json.Marshal(locData)
	request, _ := http.NewRequest("POST", url+"/location", bytes.NewBuffer(dataAsString))

	client.Do(request)
}

func RequestAccessToken(url, id, hashedPassword string) string {
	client := &http.Client{}
	atData := DataPackage{IDT: id, Data: hashedPassword}
	dataAsString, _ := json.Marshal(atData)
	request, _ := http.NewRequest("PUT", url+"/requestAccess", bytes.NewBuffer(dataAsString))

	r, _ := client.Do(request)
	defer r.Body.Close()
	var device DataPackage
	json.NewDecoder(r.Body).Decode(&device)
	return string(device.Data)
}
