package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

const configFile = "config.json"

func WriteSettings(settings Settings) {
	data, _ := json.Marshal(settings)
	write(configFile, data)
}

func ReadSettings() Settings {
	data := read(configFile)
	if data == nil {
		return nil
	}
	var settings Settings
	err := json.Unmarshal(data, &settings)
	if err != nil {
		return nil
	}
	return settings
}

func read(file string) []byte {
	data, err := os.ReadFile(file)
	if err != nil {
		return nil
	}
	return data
}

func write(file string, data []byte) {
	ioutil.WriteFile(file, data, 0777)
}
