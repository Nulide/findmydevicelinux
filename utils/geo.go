package utils

import (
	"log"
	"time"

	"github.com/maltegrosse/go-geoclue2"
)

type Location struct {
	Provider string
	Lat      float64
	Long     float64
	Time     int64
}

func GetLocation() Location {
	gcm, err := geoclue2.NewGeoclueManager()
	if err != nil {
		log.Fatal(err.Error())
	}
	client, err := gcm.GetClient()
	if err != nil {
		log.Fatal(err.Error())
	}
	err = client.SetDesktopId("firefox")
	if err != nil {
		log.Fatal(err.Error())
	}
	err = client.SetRequestedAccuracyLevel(geoclue2.GClueAccuracyLevelExact)
	if err != nil {
		log.Fatal(err.Error())
	}
	err = client.Start()
	if err != nil {
		log.Fatal(err.Error())
	}
	location, err := client.GetLocation()
	if err != nil {
		log.Fatal(err.Error())
	}
	latitude, err := location.GetLatitude()
	if err != nil {
		log.Fatal(err.Error())
	}
	longitude, err := location.GetLongitude()
	if err != nil {
		log.Fatal(err.Error())
	}
	time := time.Now().UnixMicro()

	loc := Location{Provider: "network", Lat: latitude, Long: longitude, Time: time}

	return loc
}
