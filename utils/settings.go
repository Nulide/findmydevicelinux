package utils

import "strconv"

type Settings map[int]string

const SET_FMDSERVER_UPLOAD_SERVICE = 101
const SET_FMDSERVER_URL = 102
const SET_FMDSERVER_UPDATE_TIME = 103
const SET_FMDSERVER_ID = 104
const SET_FMDSERVER_PASSWORD_SET = 105
const SET_FMDSERVER_LOCATION_TYPE = 106 // 0=GPS, 1=CELL, 2=ALL
const SET_FMDSERVER_AUTO_UPLOAD = 107

const SET_FMD_CRYPT_PUBKEY = 601
const SET_FMD_CRYPT_PRIVKEY = 602
const SET_FMD_CRYPT_HPW = 603

func InitSettings() Settings {
	settings := ReadSettings()
	if settings != nil {
		return settings
	}
	return Settings{}
}

func (s *Settings) PutInt(setting int, value int) {
	val := strconv.Itoa(value)
	s.Put(setting, val)
}

func (s *Settings) PutBoolean(setting int, value bool) {
	if value {
		s.Put(setting, "True")
	} else {
		s.Put(setting, "False")
	}
}

func (s *Settings) Put(setting int, value string) {
	(*s)[setting] = value
	WriteSettings(*s)
}

func (s *Settings) GetInt(setting int) int {
	value, hasValue := (*s)[setting]
	if hasValue {
		val, _ := strconv.Atoi(value)
		return val
	}

	switch setting {
	case SET_FMDSERVER_UPDATE_TIME:
		return 60
	}
	return -1
}

func (s *Settings) GetBoolean(setting int) bool {
	value, hasValue := (*s)[setting]
	if hasValue {
		if value == "True" {
			return true
		} else {
			return false
		}
	}
	switch setting {
	case SET_FMDSERVER_UPLOAD_SERVICE:
		return false
	case SET_FMDSERVER_AUTO_UPLOAD:
		return false
	case SET_FMDSERVER_PASSWORD_SET:
		return false
	}
	return false
}

func (s *Settings) GetString(setting int) string {
	value, hasValue := (*s)[setting]
	if hasValue {
		return value
	}
	switch setting {
	case SET_FMDSERVER_URL:
		return "https://fmd.nulide.de:1020"
	}
	return ""
}
