package main

import (
	"bufio"
	"findmydevicelinux/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-co-op/gocron"
)

var version = "v0.0.1"

func main() {
	fmt.Println("FindMyDevice")
	fmt.Println(version)

	set := utils.InitSettings()
	if set.GetString(utils.SET_FMD_CRYPT_PUBKEY) == "" {
		fmt.Println("Enter your new password")
		reader := bufio.NewReader(os.Stdin)
		pass, _ := reader.ReadString('\n')
		pass = strings.Replace(pass, "\n", "", -1)

		fmt.Println("Creating Keys")
		priv, pub := utils.CreateKeys()

		fmt.Println("Encrypting Keys")
		encryptedPrivateKey := utils.AESEncrypt(priv, pass)
		hashedPW := utils.HashedPassword(pass)

		set.Put(utils.SET_FMD_CRYPT_PUBKEY, pub)
		set.Put(utils.SET_FMD_CRYPT_PRIVKEY, encryptedPrivateKey)
		set.Put(utils.SET_FMD_CRYPT_HPW, hashedPW)

		fmt.Println("Requesting id")
		id := utils.RegisterDevice(set.GetString(utils.SET_FMDSERVER_URL), pub, encryptedPrivateKey, hashedPW)
		fmt.Println(id)
		set.Put(utils.SET_FMDSERVER_ID, id)
	}

	handle(set)

	updateTime := set.GetInt(utils.SET_FMDSERVER_UPDATE_TIME)

	s := gocron.NewScheduler(time.UTC)

	s.Every(updateTime).Minute().Do(handle, set)

	s.StartBlocking()

	fmt.Println("Demo end")

}

func handle(set utils.Settings) {
	fmt.Println("Requesting AT")
	at := utils.RequestAccessToken(set.GetString(utils.SET_FMDSERVER_URL), set.GetString(utils.SET_FMDSERVER_ID), set.GetString(utils.SET_FMD_CRYPT_HPW))

	fmt.Println("Getting location")
	loc := utils.GetLocation()

	fmt.Println(loc.Lat)
	fmt.Println(loc.Long)

	fmt.Println("Sending location")
	pub := set.GetString(utils.SET_FMD_CRYPT_PUBKEY)
	provider := utils.EncodeBase64(utils.RsaEncrypt("geoclue2", pub))
	bat := utils.EncodeBase64(utils.RsaEncrypt(strconv.Itoa(utils.GetBatPercentage()), pub))
	lon := utils.EncodeBase64(utils.RsaEncrypt(fmt.Sprint(loc.Long), pub))
	lat := utils.EncodeBase64(utils.RsaEncrypt(fmt.Sprint(loc.Lat), pub))
	date := time.Now().UnixMilli()

	utils.PostLocation(set.GetString(utils.SET_FMDSERVER_URL), at, provider, bat, lon, lat, date)
}
