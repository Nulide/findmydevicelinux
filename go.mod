module findmydevicelinux

go 1.17

require github.com/maltegrosse/go-geoclue2 v0.1.1

require (
	github.com/godbus/dbus/v5 v5.0.3 // indirect
	golang.org/x/crypto v0.0.0-20220313003712-b769efc7c000 // indirect
)
